# base image
FROM node:18-alpine

# Set the working directory for the rest of the commands
USER node
RUN mkdir /home/node/app
WORKDIR /home/node/app

# Copy the package.json and package-lock.json files to the working directory
COPY --chown=node:node package*.json ./

# Install dependencies
RUN npm ci

# Copy the rest of the source code to the working directory
COPY --chown=node:node . .