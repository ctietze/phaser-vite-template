/* eslint-disable no-unused-vars */
export enum SCENES {
  LOAD = 'LoadScene',
  MENU = 'MenuScene',
  GAME = 'GameScene',
  UI = 'UiScene',
}

export enum TEXTURES {
  BUTTON = 'button',
  BUTTON_PLAY = 'button_play',
}

export const COLORS = {
  BACKGROUND: new Phaser.Display.Color(18, 3, 48, 255),
  TEXT: new Phaser.Display.Color(255, 255, 255, 255),
  PRIMARY: new Phaser.Display.Color(242, 19, 183, 255),
};
