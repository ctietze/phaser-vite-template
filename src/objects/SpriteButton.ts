export default class SpriteButton extends Phaser.GameObjects.Sprite {
  private onPress: Function;
  private ANIM_PRESS = 'animPress';
  private frameDefault = 0;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    onPress: Function,
    texture: string | Phaser.Textures.Texture,
    frame?: number,
  ) {
    super(scene, x, y, texture, frame);
    scene.add.existing(this);

    this.onPress = onPress;
    this.setInteractive({ useHandCursor: true });
    this.on('pointerdown', () => {
      this.onPress();
    });

    if (frame) {
      this.frameDefault = frame;
    }
  }

  setAnimationOnPress(frameStart: number, frameEnd: number) {
    this.anims.create({
      key: this.ANIM_PRESS,
      frames: this.anims.generateFrameNumbers(this.texture.key, {
        start: frameStart,
        end: frameEnd,
      }),
      frameRate: 12,
      repeat: 0,
    });
    this.removeListener('pointerdown');
    this.on('pointerdown', () => {
      this.play(this.ANIM_PRESS);
    });
    this.on('animationcomplete', (animation: Phaser.Animations.Animation) => {
      switch (animation.key) {
        case this.ANIM_PRESS:
          this.onPress();
          break;
      }
    });

    return this;
  }

  setAnimationOnHover(
    frameHover: number,
    frameDefault: number = this.frameDefault,
  ) {
    this.on('pointerover', () => {
      this.setFrame(frameHover);
    });

    this.on('pointerout', () => {
      this.setFrame(frameDefault);
    });

    return this;
  }
}
