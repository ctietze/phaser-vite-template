export default class LoadingBar {
  private scene: Phaser.Scene;
  private x: number;
  private y: number;
  private width: number;
  private height: number;
  private backgroundColor = 0x222222;
  private backgroundRect!: Phaser.GameObjects.Graphics;
  private barColor = 0xcccccc;
  private barRect!: Phaser.GameObjects.Graphics;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    width: number,
    height: number,
  ) {
    this.scene = scene;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.createGraphics();
  }

  /**
   * Fill the loading bar to given percentage
   * @param percentage Number from 0 to 1
   */
  fillBar(percentage: number) {
    const padding = 10;
    const width = this.width - padding * 2;
    const height = this.height - padding * 2;

    this.barRect.fillRect(
      this.x + padding,
      this.y + padding,
      width * percentage,
      height,
    );
  }

  /**
   * Create two rectangles. Backgorund and loading bar.
   */
  private createGraphics() {
    this.backgroundRect = new Phaser.GameObjects.Graphics(this.scene, {
      fillStyle: {
        color: this.backgroundColor,
      },
    });
    this.backgroundRect.fillRect(this.x, this.y, this.width, this.height);
    this.scene.add.existing(this.backgroundRect);

    this.barRect = new Phaser.GameObjects.Graphics(this.scene, {
      fillStyle: {
        color: this.barColor,
      },
    });
    this.scene.add.existing(this.barRect);
  }
}
